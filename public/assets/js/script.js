import * as THREE from "three";
import { STLLoader } from 'three/examples/jsm/loaders/STLLoader';

var sock = io.connect("/ip");

let contener_modif_element = document.querySelector('#contener_modif_element');
let remove_element = document.querySelector('#remove_element');
let map = document.querySelector('#map');
let currentPlayer = document.querySelector('#player');
let x_value = document.querySelector('#x_value');
let y_value = document.querySelector('#y_value');
let z_value = document.querySelector('#z_value');

let mapSize = {
  width: 500,
  height: 500
};

map.style.width = (mapSize.width * 4) / 11 + "px";
map.style.height = (mapSize.height * 3) / 11 + "px";

let params = {
  color: "white", 
  metalness: 1, 
  roughness: .6
};

let focus = false;
let elementClicked = false;

let contener_map_modif = document.querySelector('#contener_map_modif');
let map_number1 = document.querySelector('#map_number1');

let hover_infos = document.querySelector('#hover_infos');
let number_map = document.querySelector('#number_map');
let number_map_x = document.querySelector('#number_map_x');
let number_map_y = document.querySelector('#number_map_y');
let map_x_value, map_y_value;
let numberX, numberY;

let column = document.querySelectorAll(".column");
column.forEach(col => {
  col.addEventListener('mouseover', event =>{
    hover_infos.style.display = "block";
  })
});

map.addEventListener('mouseover', event =>{
  hover_infos.style.display = "block";
  hover_infos.style.left = event.pageX - hover_infos.offsetWidth - 5 + "px";
  hover_infos.style.top = event.pageY + "px"; 
})

map.addEventListener('click', event =>{
  contener_map_modif.style.display = "block";
  contener_modif_element.style.display = "none";
  if ( map_x_value < map.offsetWidth / 4 && map_y_value < map.offsetHeight / 3) { 
    mapNumber("map1")
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 2 && map_y_value < map.offsetHeight / 3) { 
    mapNumber("map2")
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 3 && map_y_value < map.offsetHeight / 3) { 
    mapNumber("map3")
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 4 && map_y_value < map.offsetHeight / 3) { 
    mapNumber("map4")
  }
  else if ( map_x_value < map.offsetWidth / 4 && map_y_value < map.offsetHeight / 1.5) { 
    mapNumber("map5")
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 2 && map_y_value < map.offsetHeight / 1.5) { 
    mapNumber("map6")
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 3 && map_y_value < map.offsetHeight / 1.5) { 
    mapNumber("map7")
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 4 && map_y_value < map.offsetHeight / 1.5) { 
    mapNumber("map8")
  }
  else if ( map_x_value < map.offsetWidth / 4 && map_y_value < map.offsetHeight) { 
    mapNumber("map9")
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 2 && map_y_value < map.offsetHeight) { 
    mapNumber("map10")
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 3 && map_y_value < map.offsetHeight) { 
    mapNumber("map11")
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 4 && map_y_value < map.offsetHeight) { 
    mapNumber("map12")
  }

  for (let x = 0; x < allFloor.children.length; x++) {
    if (allFloor.children[x].name === map_number1.innerHTML) {
      mapModifDisp.value = Math.round(allFloor.children[x].material.displacementScale);     
    };       
  };
});

function mapNumber(value) {
  map_number1.innerHTML = value;
}

map.addEventListener('mousemove', event =>{
  hover_infos.style.left = event.pageX - hover_infos.offsetWidth - 5 + "px";
  hover_infos.style.top = event.pageY + "px"; 

  const target = event.target;
  const rect = target.getBoundingClientRect();

  map_x_value = Math.round(event.pageX - rect.left);
  map_y_value = Math.round(event.pageY - rect.top);

  if (map_x_value < map.offsetWidth / 2) {
    numberX = Math.round(- (mapSize.width * 2) + (map_x_value * (mapSize.width * 2) / (map.offsetWidth / 2)));
    number_map_x.innerHTML = numberX;
  }
  else{
    numberX = Math.round((map_x_value * (mapSize.width * 2) / (map.offsetWidth / 2)) - (mapSize.width * 2));
    number_map_x.innerHTML = numberX;
  }

  if (map_y_value < map.offsetHeight / 2) {
    numberY = Math.round((mapSize.height * 1.5) - (map_y_value * (mapSize.height * 1.5) / (map.offsetHeight / 2)));
    number_map_y.innerHTML = numberY; 
  }
  else{
    numberY = Math.round((mapSize.height * 1.5) - (map_y_value * (mapSize.height * 1.5) / (map.offsetHeight / 2)));
    number_map_y.innerHTML = numberY; 
  }

  number_map.innerHTML = numberX;

  if ( map_x_value < map.offsetWidth / 4 && map_y_value < map.offsetHeight / 3) { 
    number_map.innerHTML = "map1";
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 2 && map_y_value < map.offsetHeight / 3) { 
    number_map.innerHTML = "map2";
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 3 && map_y_value < map.offsetHeight / 3) { 
    number_map.innerHTML = "map3";
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 4 && map_y_value < map.offsetHeight / 3) { 
    number_map.innerHTML = "map4";
  }
  else if ( map_x_value < map.offsetWidth / 4 && map_y_value < map.offsetHeight / 1.5) { 
    number_map.innerHTML = "map5";
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 2 && map_y_value < map.offsetHeight / 1.5) { 
    number_map.innerHTML = "map6";
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 3 && map_y_value < map.offsetHeight / 1.5) { 
    number_map.innerHTML = "map7";
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 4 && map_y_value < map.offsetHeight / 1.5) { 
    number_map.innerHTML = "map8";
  }
  else if ( map_x_value < map.offsetWidth / 4 && map_y_value < map.offsetHeight) { 
    number_map.innerHTML = "map9";
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 2 && map_y_value < map.offsetHeight) { 
    number_map.innerHTML = "map10";
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 3 && map_y_value < map.offsetHeight) { 
    number_map.innerHTML = "map11";
  }
  else if ( map_x_value < (map.offsetWidth / 4) * 4 && map_y_value < map.offsetHeight) { 
    number_map.innerHTML = "map12";
  }
})

let open = document.querySelector('#open');
let close = document.querySelector('#close');
let contener_rapel = document.querySelector('#contener_rapel');
open.addEventListener('click', event => {
  contener_rapel.style.display = "block";
  open.style.display = "none";
})

close.addEventListener('click', event => {
  contener_rapel.style.display = "none";
  open.style.display = "flex";
})


map.addEventListener('mouseleave', event =>{
  hover_infos.style.display = "none";
})

const userIdentifier = parseInt((1 + Math.floor(Math.random() * 8)) + "" + (1 + Math.floor(Math.random() * 8)) + "" + (1 + Math.floor(Math.random() * 8)) + "" + (1 + Math.floor(Math.random() * 8)) + "" + (1 + Math.floor(Math.random() * 8)) + "" + (1 + Math.floor(Math.random() * 8)) + "" + (1 + Math.floor(Math.random() * 8)) + "" + (1 + Math.floor(Math.random() * 8)) + (1 + Math.floor(Math.random() * 8)) + "" + (1 + Math.floor(Math.random() * 8)));
sock.emit('createJsonDocument', userIdentifier);
console.log("userId:" + userIdentifier);

let inputss = document.querySelectorAll('.modifier');

// GET ARRAY OF JSON FILES --------------------------------------------------------------------------------------------------------------------------------------
let arrayJson;
sock.on("sendArrayFiles", sendArrayFiles);
function sendArrayFiles(data) {
  arrayJson = data;
  for (const jsonFile of arrayJson) {
    fetch(jsonFile)
    .then((response) => response.json())
    .then(function (json) {
      for (let x = 0; x < json.basic.length; x++) {
        addFromJson(
          json.basic[x].user_identifier,
          json.basic[x].identifier,
          json.basic[x].basicOrModel,
          json.basic[x].geometry,
          json.basic[x].material,
          json.basic[x].xPos,
          json.basic[x].yPos,
          json.basic[x].zPos,
          json.basic[x].xRot,
          json.basic[x].yRot,
          json.basic[x].zRot,
          json.basic[x].xSca,
          json.basic[x].ySca,
          json.basic[x].zSca
        );
      }
    });
  }
}

// SCENE ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const canvas = document.querySelector("canvas.webgl");
const scene = new THREE.Scene();

let color_back_fog = "rgb(180, 180, 180)";
scene.background = new THREE.Color(color_back_fog);
scene.fog = new THREE.Fog(color_back_fog, 300, 600);

// LOADERS ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const stlLoader = new STLLoader();
const textureLoader = new THREE.TextureLoader();

// GROUP ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const allGroupp = new THREE.Group();
const allFloor = new THREE.Group();

// GEOMETRY AND MATERIAL ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const terrainTextureColor = textureLoader.load('assets/textures/map/map_color.png')
const terrainTextureDisp = textureLoader.load('assets/textures/map/map_disp.png')
const terrainTextureNorm = textureLoader.load('assets/textures/map/map_norm.jpg')
const terrainTextureOcc = textureLoader.load('assets/textures/map/map_occ.jpg')
const terrainTextureSpec = textureLoader.load('assets/textures/map/map_spec.jpg')

const texture1 = textureLoader.load('assets/textures/objects/texture1.png')
const texture2 = textureLoader.load('assets/textures/objects/texture2.png')
const texture3 = textureLoader.load('assets/textures/objects/texture3.png')

texture1.repeat.x = 2;
texture1.repeat.y = 2;
texture1.wrapS = THREE.MirroredRepeatWrapping;
texture1.wrapT = THREE.MirroredRepeatWrapping;

texture2.repeat.x = 3;
texture2.repeat.y = 3;
texture2.wrapS = THREE.MirroredRepeatWrapping;
texture2.wrapT = THREE.MirroredRepeatWrapping;

texture3.repeat.x = 5;
texture3.repeat.y = 5;
texture3.wrapS = THREE.MirroredRepeatWrapping;
texture3.wrapT = THREE.MirroredRepeatWrapping;

terrainTextureColor.repeat.x = 1;
terrainTextureColor.repeat.y = 1;
terrainTextureColor.wrapS = THREE.MirroredRepeatWrapping;
terrainTextureColor.wrapT = THREE.MirroredRepeatWrapping;
terrainTextureNorm.repeat.x = 1;
terrainTextureNorm.repeat.y = 1;
terrainTextureNorm.wrapS = THREE.MirroredRepeatWrapping;
terrainTextureNorm.wrapT = THREE.MirroredRepeatWrapping;
terrainTextureDisp.repeat.x = 1;
terrainTextureDisp.repeat.y = 1;
terrainTextureDisp.wrapS = THREE.MirroredRepeatWrapping;
terrainTextureDisp.wrapT = THREE.MirroredRepeatWrapping;

let box = new THREE.BoxGeometry( 2, 2, 1, 10, 10, 10);
let sphere = new THREE.SphereGeometry( 1, 16 , 10);
let capsule = new THREE.CapsuleGeometry( 1, 1, 3, 12);
let torus_knot = new THREE.TorusKnotGeometry( 1, 0.4, 40, 40, 5, 3 );
let ico = new THREE.IcosahedronGeometry(2, 0);
let cone = new THREE.ConeGeometry(1, 7, 12, 10);
let cylinder = new THREE.CylinderGeometry( 1, 1, 7, 12, 10); 
let octa = new THREE.OctahedronGeometry(2, 0);
let plane = new THREE.PlaneGeometry(4, 4, 10, 10);
let tetra = new THREE.TetrahedronGeometry(2, 0);
let torus = new THREE.TorusGeometry(1, .4, 8, 10);
let arrayBasic = ["box", "sphere", "capsule", "torus_knot", "ico", "cone", "cylinder", "octa", "plane", "tetra", "torus"];

let normalMaterial = new THREE.MeshNormalMaterial({flatShading: true});
let normalMaterial2 = new THREE.MeshNormalMaterial({displacementMap: texture2, displacementScale: 0, flatShading: true,  side: THREE.DoubleSide});
let normalMaterial3 = new THREE.MeshNormalMaterial({displacementMap: texture2, displacementScale: 0, flatShading: true,  side: THREE.DoubleSide});

let mapTexture1;
let mapTexture2;
let mapTexture3;

let red = new THREE.MeshStandardMaterial({color: "red", emissive: "red", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let red2 = new THREE.MeshStandardMaterial({color: "red", emissive: "red", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let red3 = new THREE.MeshStandardMaterial({color: "red", emissive: "red", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let red4 = new THREE.MeshStandardMaterial({color: "red", emissive: "red", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});
let coral = new THREE.MeshStandardMaterial({color: "rgb(255, 74, 149)", emissive: "rgb(255, 74, 149)", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let coral2 = new THREE.MeshStandardMaterial({color: "rgb(255, 74, 149)", emissive: "rgb(255, 74, 149)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let coral3 = new THREE.MeshStandardMaterial({color: "rgb(255, 74, 149)", emissive: "rgb(255, 74, 149)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let coral4 = new THREE.MeshStandardMaterial({color: "rgb(255, 74, 149)", emissive: "rgb(255, 74, 149)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});

let green = new THREE.MeshStandardMaterial({color: "rgb(3, 69, 0)",  emissive: "rgb(3, 69, 0)", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let green2 = new THREE.MeshStandardMaterial({color: "rgb(3, 69, 0)", emissive: "rgb(3, 69, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let green3 = new THREE.MeshStandardMaterial({color: "rgb(3, 69, 0)", emissive: "rgb(3, 69, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let green4 = new THREE.MeshStandardMaterial({color: "rgb(3, 69, 0)", emissive: "rgb(3, 69, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});
let lightgreen = new THREE.MeshStandardMaterial({color: "rgb(11, 255, 0)", emissive: "rgb(11, 255, 0)", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let lightgreen2 = new THREE.MeshStandardMaterial({color: "rgb(11, 255, 0)", emissive: "rgb(11, 255, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let lightgreen3 = new THREE.MeshStandardMaterial({color: "rgb(11, 255, 0)", emissive: "rgb(11, 255, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let lightgreen4 = new THREE.MeshStandardMaterial({color: "rgb(11, 255, 0)", emissive: "rgb(11, 255, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});

let blue = new THREE.MeshStandardMaterial({color: "rgb(18, 22, 255)",  emissive: "rgb(18, 22, 255)", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let blue2 = new THREE.MeshStandardMaterial({color: "rgb(18, 22, 255)", emissive: "rgb(18, 22, 255)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let blue3 = new THREE.MeshStandardMaterial({color: "rgb(18, 22, 255)", emissive: "rgb(18, 22, 255)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let blue4 = new THREE.MeshStandardMaterial({color: "rgb(18, 22, 255)", emissive: "rgb(18, 22, 255)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});
let lightblue = new THREE.MeshStandardMaterial({color: "rgb(51, 126, 245)", emissive: "rgb(51, 126, 245)", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let lightblue2 = new THREE.MeshStandardMaterial({color: "rgb(51, 126, 245)", emissive: "rgb(51, 126, 245)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let lightblue3 = new THREE.MeshStandardMaterial({color: "rgb(51, 126, 245)", emissive: "rgb(51, 126, 245)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let lightblue4 = new THREE.MeshStandardMaterial({color: "rgb(51, 126, 245)", emissive: "rgb(51, 126, 245)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});

let purple = new THREE.MeshStandardMaterial({color: "rgb(101, 0, 201)",  emissive: "rgb(101, 0, 201)", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let purple2 = new THREE.MeshStandardMaterial({color: "rgb(101, 0, 201)", emissive: "rgb(101, 0, 201)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let purple3 = new THREE.MeshStandardMaterial({color: "rgb(101, 0, 201)", emissive: "rgb(101, 0, 201)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let purple4 = new THREE.MeshStandardMaterial({color: "rgb(101, 0, 201)", emissive: "rgb(101, 0, 201)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});
let violet = new THREE.MeshStandardMaterial({color: "rgb(214, 33, 255)", emissive: "rgb(214, 33, 255)", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let violet2 = new THREE.MeshStandardMaterial({color: "rgb(214, 33, 255)", emissive: "rgb(214, 33, 255)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let violet3 = new THREE.MeshStandardMaterial({color: "rgb(214, 33, 255)", emissive: "rgb(214, 33, 255)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let violet4 = new THREE.MeshStandardMaterial({color: "rgb(214, 33, 255)", emissive: "rgb(214, 33, 255)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});

let orange = new THREE.MeshStandardMaterial({color: "rgb(207, 79, 0)",  emissive: "rgb(207, 79, 0)", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let orange2 = new THREE.MeshStandardMaterial({color: "rgb(207, 79, 0)", emissive: "rgb(207, 79, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let orange3 = new THREE.MeshStandardMaterial({color: "rgb(207, 79, 0)", emissive: "rgb(207, 79, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let orange4 = new THREE.MeshStandardMaterial({color: "rgb(207, 79, 0)", emissive: "rgb(207, 79, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});
let yellow = new THREE.MeshStandardMaterial({color: "rgb(238, 255, 0)", emissive: "rgb(238, 255, 0)", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let yellow2 = new THREE.MeshStandardMaterial({color: "rgb(238, 255, 0)", emissive: "rgb(238, 255, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let yellow3 = new THREE.MeshStandardMaterial({color: "rgb(238, 255, 0)", emissive: "rgb(238, 255, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let yellow4 = new THREE.MeshStandardMaterial({color: "rgb(238, 255, 0)", emissive: "rgb(238, 255, 0)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});

let black = new THREE.MeshStandardMaterial({color: "rgb(20, 20, 20)",  emissive: "rgb(20, 20, 20)", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let black2 = new THREE.MeshStandardMaterial({color: "rgb(20, 20, 20)", emissive: "rgb(20, 20, 20)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let black3 = new THREE.MeshStandardMaterial({color: "rgb(20, 20, 20)", emissive: "rgb(20, 20, 20)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let black4 = new THREE.MeshStandardMaterial({color: "rgb(20, 20, 20)", emissive: "rgb(20, 20, 20)", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});

let gray = new THREE.MeshStandardMaterial({color: "gray", emissive: "gray", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let gray2 = new THREE.MeshStandardMaterial({color: "gray", emissive: "gray", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let gray3 = new THREE.MeshStandardMaterial({color: "gray", emissive: "gray", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let gray4 = new THREE.MeshStandardMaterial({color: "gray", emissive: "gray", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});

let white = new THREE.MeshStandardMaterial({color: "white", emissive: "white", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});
let white2 = new THREE.MeshStandardMaterial({color: "white", emissive: "white", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture1, displacementScale: 2, side: THREE.DoubleSide});
let white3 = new THREE.MeshStandardMaterial({color: "white", emissive: "white", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture2, displacementScale: 2, side: THREE.DoubleSide});
let white4 = new THREE.MeshStandardMaterial({color: "white", emissive: "white", metalness: params.metalness, roughness: params.roughness, map: false, flatShading: true, displacementMap: texture3, displacementScale: 2, side: THREE.DoubleSide});

let lightgray = new THREE.MeshStandardMaterial({color: "lightgray", metalness: params.metalness, roughness: params.roughness, flatShading: true, displacementScale: 0, side: THREE.DoubleSide});

red2.wichDisplacementMap = "texture1";
red3.wichDisplacementMap = "texture2";
red4.wichDisplacementMap = "texture3";

let standardMaterial = new THREE.MeshStandardMaterial({color: params.color});
normalMaterial.side = THREE.DoubleSide;

// ADD ELEMENT ON ALL CONNECTED DEVICES ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
sock.on("addElement", addElement);
function addElement(data) {
  let elementGeometryyy = eval(data.geometry);
  let elementMaterialll = eval(data.material);
  let element = new THREE.Mesh(elementGeometryyy, elementMaterialll);
  element.position.set(data.xPos, data.yPos, data.zPos);
  element.rotation.set(data.xRot, data.yRot, data.zRot);
  element.scale.set(data.xSca, data.ySca, data.zSca);
  element.name = data.identifier;
  element.wichUser = data.user_identifier;
  element.wichColor = data.material;
  element.basicOrModel = data.basicOrModel;
  allGroupp.add(element);
}

// REMOVE ELEMENT ON ALL CONNECTED DEVICES ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
sock.on("removeElementEverywhere", removeElement);
function removeElement(data) {
  for (let x = 0; x < allGroupp.children.length; x++) {
    if (allGroupp.children[x].name === data) {
      allGroupp.children[x].parent.remove(allGroupp.children[x]);
    }
  }
}

// CHANGE ELEMENT ON ALL CONNECTED DEVICES ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
sock.on("changePosition", changePosition);
function changePosition(data) {
  for (let x = 0; x < allGroupp.children.length; x++) {
    if (allGroupp.children[x].name === data.identifier) {
      if (data.property === "xPos") {                  
        new TWEEN.Tween(allGroupp.children[x].position).to({x: data.property1}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
      }
      else if (data.property === "yPos") {                  
        new TWEEN.Tween(allGroupp.children[x].position).to({y: data.property1}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();        
      }
      else if (data.property === "zPos") {                  
        new TWEEN.Tween(allGroupp.children[x].position).to({z: data.property1}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();        
      }
      else if (data.property === "xRot") {                  
        new TWEEN.Tween(allGroupp.children[x].rotation).to({x: data.property1}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();        
      }
      else if (data.property === "yRot") {                  
        new TWEEN.Tween(allGroupp.children[x].rotation).to({y: data.property1}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();        
      }
      else if (data.property === "zRot") {                  
        new TWEEN.Tween(allGroupp.children[x].rotation).to({z: data.property1}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();        
      }
      else if (data.property === "xSca") {                  
        new TWEEN.Tween(allGroupp.children[x].scale).to({x: data.property1}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();        
      }
      else if (data.property === "ySca") {                  
        new TWEEN.Tween(allGroupp.children[x].scale).to({y: data.property1}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();        
      }
      else if (data.property === "zSca") {                  
        new TWEEN.Tween(allGroupp.children[x].scale).to({z: data.property1}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();        
      }
      else if (data.property === "material") {                  
        allGroupp.children[x].material = eval(data.property1);
        allGroupp.children[x].wichColor = data.property1;
      }
    }    
  }

  if (data.user_identifier === "map") {      
    for (let x = 0; x < allFloor.children.length; x++) {
      if (data.identifier === allFloor.children[x].name) {
        new TWEEN.Tween(allFloor.children[x].material).to({displacementScale: data.property1}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
      }
    }            
  }
}

// FLOOR ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
let xMapCoordinate = 750;
let zMapCoordinate = 500;
let arrayRandColor = [];
let numberFloor = 1;

fetch('./data/map.json')
.then((response) => response.json())
.then(function (json) {
  for (let x = 0; x < json.basic.length; x++) {
    numberFloor++;
    if (x === 4 || x === 8) {
      xMapCoordinate = 749
      zMapCoordinate -= 498;
    }

    let floorGeometry = new THREE.PlaneGeometry(mapSize.width, mapSize.height, mapSize.width, mapSize.height);
    let floorMaterial = new THREE.MeshStandardMaterial({ color: json.basic[x].material, emissive: "rgb(0, 0, 0)", flatShading: true, aoMap: terrainTextureOcc, roughnessMap: terrainTextureSpec, displacementMap: terrainTextureDisp, displacementScale: json.basic[x].displacementScale, normalMap: terrainTextureNorm, displacementBias: 0 });
    floorMaterial.side = THREE.DoubleSide;
    floorMaterial.metalness = .7
    floorMaterial.roughness = .6
    let floorr = new THREE.Mesh(floorGeometry, floorMaterial);
    floorr.name = json.basic[x].identifier;
    floorr.basicOrModel = json.basic[x].basicOrModel;
    floorr.wichColor = floorMaterial;

    floorr.position.set(xMapCoordinate, -15, zMapCoordinate);
    floorr.rotateX(Math.PI * -0.5);

    allFloor.add(floorr);
    xMapCoordinate -= 498;
    }
  });

  scene.add(allFloor);

// CHANGE ELEMENT COLOR WHEN CLICKED ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
let activeElementMaterial = new THREE.MeshStandardMaterial({ color: "yellow" });
let activeOtherElementMaterial = new THREE.MeshStandardMaterial({ color: "yellow" });
let globalMaterial = new THREE.MeshNormalMaterial();

// ADD NEW ELEMENT ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
let add_element = document.querySelector('#add_element');
let wich_form = "sphere";

add_element.addEventListener('change', event =>{
  wich_form = add_element.value;
  createElement(
    wich_form,
    "white"
  )
  document.querySelector('#no_value_form').selected = true;
  add_element.blur();
})

// RAYCASTER, CHECK IF ELEMENT IS CLICKED ------------------------------------------------------------------------------------------------
const raycaster = new THREE.Raycaster();
let currentIntersect = null;
const rayOrigin = new THREE.Vector3(-3, 0, 0);
const rayDirection = new THREE.Vector3(10, 0, 0);
rayDirection.normalize();

const mouse = new THREE.Vector2();
let controllerPositionX,
  controllerPositionY,
  controllerPositionZ,
  controllerRotationX,
  controllerRotationY,
  controllerRotationZ,
  controllerScaleX,
  controllerScaleY,
  controllerScaleZ,
  controllerColor, 
  controllerWidthSegments,
  controllerHeightSegments
  ;

window.addEventListener("mousemove", (event) => {
  mouse.x = (event.clientX / sizes.width) * 2 - 1;
  mouse.y = -(event.clientY / sizes.height) * 2 + 1;
});


let currentMaterial;
let currentIntersectName;

let input_initial_value;
let inputsss = document.querySelectorAll('.modifier');
let increment = document.querySelectorAll('.incr');
let decrement = document.querySelectorAll('.decr');
let option_list_color = document.querySelectorAll('.option_list_color');
// EVENT TO CHANGE ELEMENT POSITION, ROTATION AND SCALE ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
let send_value;

canvas.addEventListener("click", () => {
  currentIntersectName = null;
  for (let x = 0; x < allGroupp.children.length; x++) {
    allGroupp.children[x].material = eval(allGroupp.children[x].wichColor);

    if (currentIntersect) {
      switch (currentIntersect.object) {
        case allGroupp.children[x]:
          elementClicked = true;
          currentIntersectName = currentIntersect.object.name;

          contener_modif_element.style.display = "block";
          contener_map_modif.style.display = "none";

          if (currentIntersectName ===  allGroupp.children[x].name) {
            let actualDisplacementScale = allGroupp.children[x].material.displacementScale;
            let actualDisplacementMap = allGroupp.children[x].material.displacementMap; 

            if (
              allGroupp.children[x].wichColor === "red" ||  
              allGroupp.children[x].wichColor === "coral" ||
              allGroupp.children[x].wichColor === "blue" ||
              allGroupp.children[x].wichColor === "lightblue" ||
              allGroupp.children[x].wichColor === "green" ||
              allGroupp.children[x].wichColor === "lightgreen" ||
              allGroupp.children[x].wichColor === "purple" ||
              allGroupp.children[x].wichColor === "violet" ||
              allGroupp.children[x].wichColor === "orange" ||
              allGroupp.children[x].wichColor === "yellow" ||
              allGroupp.children[x].wichColor === "black" ||
              allGroupp.children[x].wichColor === "gray" ||
              allGroupp.children[x].wichColor === "white"
            ) {
              allGroupp.children[x].material = normalMaterial; 
            }
            else{
              normalMaterial2.displacementMap = actualDisplacementMap;
              normalMaterial2.displacementScale = actualDisplacementScale;   
              allGroupp.children[x].material = normalMaterial2;         
            }
              
            
            inputsss.forEach(input => {
              if (input.classList.contains("positionY")) {
                input.value = -Math.round(eval(input.dataset.wich_property2));     
              }
              else if (input.classList.contains("rotationX") || input.classList.contains("rotationY") || input.classList.contains("rotationZ")) {
                input.value = eval(input.dataset.wich_property2);     
              }
              else{
                input.value = Math.round(eval(input.dataset.wich_property2));     
              }
            });
          }            

          option_list_color.forEach(o_l_c => {
            if (allGroupp.children[x].wichColor === o_l_c.value) {
              o_l_c.selected = true;
            }                    
          });

          break;
      }
    }
    else{

      elementClicked = false
      allGroupp.children[x].material = eval(allGroupp.children[x].wichColor);
      contener_modif_element.style.display = "none";
      contener_map_modif.style.display = "none";

      inputsss.forEach(input => {  
        if (input.classList.contains('text')) {
          input.value = "";
        }
        else if (input.classList.contains('color')) {
          document.querySelector('#no_value_form').selected = true;
          document.querySelector('#no_value_color').selected = true;
        }

        input.blur();
        focus = false;
      });
    }
  }
});

remove_element.addEventListener('change', event =>{
  for (let x = 0; x < allGroupp.children.length; x++) {
    if (elementClicked === true && currentIntersectName === allGroupp.children[x].name) {
      if (remove_element.value === "true") {
        let send_value = {
          user_identifier: allGroupp.children[x].wichUser,
          element_name: allGroupp.children[x].name,
          basic_or_model: allGroupp.children[x].basicOrModel
        }

        contener_modif_element.style.display = "none";
    
        sock.emit('removeElement', send_value);
    
        allGroupp.children[x].parent.remove(allGroupp.children[x]);
    
        remove_element.value = "";
        remove_element.blur();
      }
    }
  };
})

send_value = {};
          
inputsss.forEach(input => {  
  increment.forEach(incr => {
    incr.addEventListener('click', event => {
      for (let x = 0; x < allGroupp.children.length; x++) {
        if (elementClicked === true && currentIntersectName === allGroupp.children[x].name) {
          if (incr.dataset.wich_property === input.dataset.wich_property) {
            let new_input_value = parseInt(input.value);
            new_input_value+=1;
            input.value = new_input_value;
            if (incr.dataset.wich_property === "positionX" || incr.dataset.wich_property === "rotationX" || incr.dataset.wich_property === "scaleX") {                  
              new TWEEN.Tween(eval(incr.dataset.prop1)).to({x: new_input_value}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
              sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, incr.dataset.prop2, new_input_value);
            }
            else if (incr.dataset.wich_property === "rotationY" || incr.dataset.wich_property === "scaleY") {
              new TWEEN.Tween(eval(incr.dataset.prop1)).to({z: new_input_value}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
              sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, incr.dataset.prop2, new_input_value);
            }  
            else if (incr.dataset.wich_property === "positionY") {
              new TWEEN.Tween(eval(incr.dataset.prop1)).to({z: -new_input_value}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
              sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, incr.dataset.prop2, -new_input_value);
            }  
            else if (incr.dataset.wich_property === "positionZ" || incr.dataset.wich_property === "rotationZ" || incr.dataset.wich_property === "scaleZ") {
              new TWEEN.Tween(eval(incr.dataset.prop1)).to({y: new_input_value}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
              sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, incr.dataset.prop2, new_input_value);
            }   
          }
        }
      };      
    })
  });

  decrement.forEach(decr => {
    decr.addEventListener('click', event => {
      for (let x = 0; x < allGroupp.children.length; x++) {
        if (elementClicked === true && currentIntersectName === allGroupp.children[x].name) {
          if (decr.dataset.wich_property === input.dataset.wich_property) {
            let new_input_value = parseInt(input.value);
            new_input_value-=1;  
            input.value = new_input_value;        
            if (decr.dataset.wich_property === "positionX" || decr.dataset.wich_property === "rotationX" || decr.dataset.wich_property === "scaleX") {                  
              new TWEEN.Tween(eval(decr.dataset.prop1)).to({x: new_input_value}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
              sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, decr.dataset.prop2, new_input_value);
            }
            else if (decr.dataset.wich_property === "rotationY" || decr.dataset.wich_property === "scaleY") {
              new TWEEN.Tween(eval(decr.dataset.prop1)).to({z: new_input_value}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
              sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, decr.dataset.prop2, new_input_value);
            }  
            else if (decr.dataset.wich_property === "positionY") {
              new TWEEN.Tween(eval(decr.dataset.prop1)).to({z: -new_input_value}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
              sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, decr.dataset.prop2, -new_input_value);
            }  
            else if (decr.dataset.wich_property === "positionZ" || decr.dataset.wich_property === "rotationZ" || decr.dataset.wich_property === "scaleZ") {
              new TWEEN.Tween(eval(decr.dataset.prop1)).to({y: new_input_value}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
              sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, decr.dataset.prop2, new_input_value);
            } 
          } 
        }
      };      
    })
  });

  input.addEventListener('click', event=>{
    for (let x = 0; x < allGroupp.children.length; x++) {
      if (elementClicked === true && currentIntersectName === allGroupp.children[x].name) {
        focus = true;
      }
    };
  });

  input.addEventListener('change', event =>{
    for (let x = 0; x < allGroupp.children.length; x++) {
      if (elementClicked === true && currentIntersectName === allGroupp.children[x].name) {
        if (input.classList.contains("positionX") || input.classList.contains("rotationX") || input.classList.contains("scaleX")) {                  
          new TWEEN.Tween(eval(input.dataset.prop1)).to({x: parseFloat(input.value)}, 750).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
          sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, input.dataset.prop2, parseFloat(input.value));
        }
        else if (input.classList.contains("rotationY") || input.classList.contains("scaleY")) {
          new TWEEN.Tween(eval(input.dataset.prop1)).to({z: parseFloat(input.value)}, 750).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
          sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, input.dataset.prop2, parseFloat(input.value));
        } 
        else if (input.classList.contains("positionY")) {
          new TWEEN.Tween(eval(input.dataset.prop1)).to({z: parseFloat(-input.value)}, 750).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
          sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, input.dataset.prop2, parseFloat(-input.value));
        }  
        else if (input.classList.contains("positionZ") || input.classList.contains("rotationZ") || input.classList.contains("scaleZ")) {
          new TWEEN.Tween(eval(input.dataset.prop1)).to({y: parseFloat(input.value)}, 750).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
          sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, input.dataset.prop2, parseFloat(input.value));
        }   
        else if (input.classList.contains("color")) {
          allGroupp.children[x].material = eval(input.value);
          allGroupp.children[x].wichColor = input.value;                  
          sendValues(allGroupp.children[x].wichUser, allGroupp.children[x].name, allGroupp.children[x].basicOrModel, "material", input.value);
          input.blur();
        }            
      }
    }; 
    focus = false;
    input.blur();
  })  
});

let mapModifDisp = document.querySelector('#mapModifDisp');
let decrMap = document.querySelector('#decrMap');
let incrMap = document.querySelector('#incrMap');

mapModifDisp.addEventListener('click', event=>{
  focus = true;
});

mapModifDisp.addEventListener('change', event => {
  for (let x = 0; x < allFloor.children.length; x++) {
    if (allFloor.children[x].name === map_number1.innerHTML) {
      new TWEEN.Tween(allFloor.children[x].material).to({displacementScale: parseInt(mapModifDisp.value)}, 750).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
      sendValues("map", allFloor.children[x].name, allFloor.children[x].basicOrModel, "displacementScale", parseInt(mapModifDisp.value));
    };       
  }; 
  focus = false;
  mapModifDisp.blur(); 
})

decrMap.addEventListener('click', event =>{
  for (let x = 0; x < allFloor.children.length; x++) {
    if (allFloor.children[x].name === map_number1.innerHTML) {
      let new_input_value = parseInt(mapModifDisp.value);
      new_input_value-=1;  
      mapModifDisp.value = new_input_value; 
      new TWEEN.Tween(allFloor.children[x].material).to({displacementScale: new_input_value}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
      sendValues("map", allFloor.children[x].name, allFloor.children[x].basicOrModel, "displacementScale", new_input_value);
    };       
  }; 
})


incrMap.addEventListener('click', event =>{
  for (let x = 0; x < allFloor.children.length; x++) {
    if (allFloor.children[x].name === map_number1.innerHTML) {
      let new_input_value = parseInt(mapModifDisp.value);
      new_input_value+=1;
      mapModifDisp.value = new_input_value;
      new TWEEN.Tween(allFloor.children[x].material).to({displacementScale: new_input_value}, 500).yoyo(false).repeat(0).easing(TWEEN.Easing.Sinusoidal.Out).start();
      sendValues("map", allFloor.children[x].name, allFloor.children[x].basicOrModel, "displacementScale", new_input_value);
    };       
  };   
})

function sendValues(
  wichUser,
  element_id,
  wichForm,
  wichProperty,
  wichProperty1,
) {
  send_value = {
    user_identifier: wichUser,
    identifier: element_id,
    basic_or_model: wichForm,
    property: wichProperty,
    property1: wichProperty1,
  };
  sock.emit("sendChangedPropertys", send_value);
}

// LIGHTS ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const directionalLight = new THREE.DirectionalLight("lightgray", 3);
scene.add(directionalLight);
directionalLight.position.set(0, 30, 0)

const directionalLight2 = new THREE.DirectionalLight("lightgray", 1);
scene.add(directionalLight2);
directionalLight2.position.set(0, 3, 0)

const ambientLight = new THREE.AmbientLight("white", 2);
scene.add(ambientLight);
ambientLight.position.set(0, 2, 0)

// SIZES ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const sizes = {
  width: window.innerWidth,
  height: window.innerHeight,
};

// CAMERA ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const camera = new THREE.PerspectiveCamera(
  75,
  sizes.width / sizes.height,
  0.1,
  700
);
camera.position.set(0, 0, 0);
camera.focus = 200
scene.add(camera);

// RESIZE EVENT ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
window.addEventListener("resize", () => {
  // Update sizes
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;

  // Update camera
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();

  // Update renderer
  renderer.setSize(sizes.width, sizes.height);
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

// RENDERER ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const renderer = new THREE.WebGLRenderer({
  canvas: canvas,
  antialias: true,
});

renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFShadowMap;
renderer.useLegacyLights = false;
renderer.toneMapping = THREE.ReinhardToneMapping;
renderer.toneMappingExposure = 1.5;
renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

// CONTROLS ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
let controls = {};
let player = {
  height: .5,
  turnSpeed: .08,
  speed: 2,
  jumpHeight: .2,
  gravity: 0.01,
  velocity: 0,
  
  playerJumps: false
};

window.addEventListener('resize', () => {
  let w = window.innerWidth,
      h = window.innerHeight;
  
  renderer.setSize(w, h);
  camera.aspect = w / h;
  camera.updateProjectionMatrix();
});

// CAMERA SETUP
camera.position.set(0, player.height, -5);
camera.lookAt(new THREE.Vector3(-0, player.height, 0));
camera.rotation.order = 'YXZ'; // the default is 'XYZ'
camera.rotation.set(0, -Math.PI / 0.5, 0)


// CONTROLS LISTENERS
document.addEventListener('keydown', ({ keyCode }) => { controls[keyCode] = true });
document.addEventListener('keyup', ({ keyCode }) => { controls[keyCode] = false });

let playerMapPositionTop;
let playerMapPositionLeft;


function control() {
  // CONTROLS 
  if(controls[38]){ // up arrow, front
    camera.position.x += Math.sin(- camera.rotation.y) * player.speed;
    camera.position.z += -Math.cos(- camera.rotation.y) * player.speed;
    updatePlayerMapPosition();
    updateCoordinate();
  }
  if(controls[40]){ // down arrow, back
    camera.position.x -= Math.sin(- camera.rotation.y) * player.speed;
    camera.position.z -= -Math.cos(- camera.rotation.y) * player.speed;
    updatePlayerMapPosition();
    updateCoordinate();
  }
  if(controls[37]){ // left arrow, left
    camera.position.x += Math.sin(- camera.rotation.y - Math.PI / 2) * player.speed;
    camera.position.z += -Math.cos(- camera.rotation.y - Math.PI / 2) * player.speed;
    updatePlayerMapPosition();
    updateCoordinate();
  }
  if(controls[39]){ // right arrow, right
    camera.position.x += Math.sin(- camera.rotation.y + Math.PI / 2) * player.speed;
    camera.position.z += -Math.cos(- camera.rotation.y + Math.PI / 2) * player.speed;
    updatePlayerMapPosition();
    updateCoordinate();
  }
  if(controls[87]){ // w, up
    camera.position.y -= -2;
    updateCoordinate();
  }
  if(controls[88]){ // x, down
    camera.position.y += -2;
    updateCoordinate();
  }
  if(controls[81]){ // q, turn camera left
    camera.rotation.y += player.turnSpeed;
    currentPlayer.style.transform = "translate(-50%, -50%) rotate( " + -Math.floor(camera.rotation.y * 90 / 1.5707963267948966)  + "deg)";
  }
  if(controls[90]){ // z, turn camera up
    camera.rotation.x += player.turnSpeed;
  }
  if(controls[68]){ // d, turn camera right
    camera.rotation.y -= player.turnSpeed;
    currentPlayer.style.transform = "translate(-50%, -50%) rotate( " + -Math.floor(camera.rotation.y * 90 / 1.5707963267948966)  + "deg)";
  }
  if(controls[83]){ // s, turn camera down
    camera.rotation.x -= player.turnSpeed;
  }
}

function updatePlayerMapPosition () {
    playerMapPositionTop = (parseFloat(map.style.height.slice(0, -2)) * (Math.round(camera.position.z * 100) / 100)) / (mapSize.height * 3);
    currentPlayer.style.top = (parseFloat(map.style.height.slice(0, -2)) / 2) + playerMapPositionTop + "px";
    playerMapPositionLeft = (parseFloat(map.style.width.slice(0, -2)) * (Math.round(camera.position.x * 100) / 100)) / (mapSize.width * 4);
    currentPlayer.style.left = (parseFloat(map.style.width.slice(0, -2)) / 2) + playerMapPositionLeft + "px";
}

function updateCoordinate(){
  x_value.innerHTML = Math.round(camera.position.x);
  y_value.innerHTML =  -Math.round(camera.position.z);
  z_value.innerHTML =  Math.round(camera.position.y);
}

function update() {
  if (focus === false) {
    control();
  }
}

function render() {
  renderer.render(scene, camera);
}

function loop() {
  requestAnimationFrame(loop);
  update();
  render();
}

loop();

// ANIMATE ELEMENTS ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const clock = new THREE.Clock();

const tick = () => {
  const elapsedTime = clock.getElapsedTime();

  // RAYCASTER ---------------------------------------------------------------------------------
  raycaster.setFromCamera(mouse, camera);
  let intersects = raycaster.intersectObjects(allGroupp.children);
  if (intersects.length) {
    if (!currentIntersect) {
      document.body.style.cursor = "pointer";
    }

    currentIntersect = intersects[0];
  } 
  else {
    if (currentIntersect) {
      document.body.style.cursor = "default";
    }

    currentIntersect = null;
  }

  TWEEN.update();
  // RAYCASTER ---------------------------------------------------------------------------------

  renderer.render(scene, camera);

  window.requestAnimationFrame(tick);
};

tick();

// FUNCTION TO ADD ELEMENT FROM JSON FILE ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function addFromJson(
  wichUser,
  wichId,
  wichForm,
  wichGeometry,
  wichMaterial,
  wichXPos,
  wichYPos,
  wichZPos,
  wichXRot,
  wichYRot,
  wichZRot,
  wichXSca,
  wichYSca,
  wichZSca
) {
  const elementGeometry = eval(wichGeometry);
  const elementMaterial = eval(wichMaterial);
  const element = new THREE.Mesh(elementGeometry, elementMaterial);
  element.wichUser = wichUser;
  element.name = wichId;
  element.basicOrModel = wichForm;
  element.wichColor = wichMaterial;
  element.position.set(wichXPos, wichYPos, wichZPos);
  element.rotation.set(wichXRot, wichYRot, wichZRot);
  element.scale.set(wichXSca, wichYSca, wichZSca);
  allGroupp.add(element);
}

// FUNCTION TO CREATE A NEW ELEMENT AND ADD TO JSON FILE ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function createElement(wichGeometry, wichMaterial) {
  const elementGeometry = eval(wichGeometry);
  const elementMaterial = eval(wichMaterial);
  const element = new THREE.Mesh(elementGeometry, elementMaterial);
  const identifier = parseInt(Math.floor(Math.random() * 9) + "" + Math.floor(Math.random() * 9) + "" + Math.floor(Math.random() * 9) + "" + Math.floor(Math.random() * 9) + "" + Math.floor(Math.random() * 9) + "" + Math.floor(Math.random() * 9) + "" + Math.floor(Math.random() * 9) + "" + Math.floor(Math.random() * 9) + Math.floor(Math.random() * 9) + "" + Math.floor(Math.random() * 9));

  let forward = new THREE.Vector3(0, 0, -5).applyQuaternion(camera.quaternion); 
  let lookat = new THREE.Vector3().copy(camera.position).add(forward);
  element.position.set(lookat.x, camera.position.y - player.height, lookat.z);
  element.scale.set(1, 1, 1);
  element.rotation.set(0, 0, 0);
  element.name = identifier;
  element.wichUser = userIdentifier;
  element.wichColor = "white";

  for (const basic of arrayBasic) {
    if(basic === wichGeometry){
      element.basicOrModel = "basic";
    }
  }

  for (const model of arrayModel) {
    if(model === wichGeometry){
      element.basicOrModel = "model";
    }
  }    

  allGroupp.add(element);

  let element_id_geometry_name = identifier + "Geometry";
  let element_id_material_name = identifier + "Material";
  let value_element = {
    user_identifier: userIdentifier,
    identifier: identifier,
    basicOrModel: element.basicOrModel,
    geometry_name: element_id_geometry_name,
    material_name: element_id_material_name,
    geometry: wichGeometry,
    material: wichMaterial,
    xPos: element.position.x,
    yPos: element.position.y,
    zPos: element.position.z,
    xRot: 0,
    yRot: 0,
    zRot: 0,
    xSca: 1,
    ySca: 1,
    zSca: 1,
  };

  sock.emit("sendElementData", value_element);
}

function inputLenght(targetLenght, targetSize) {
  if (targetLenght <= 1) {
    targetSize = 1;
  }
  else{
    targetSize = targetLenght;
  } 
}

function tweenAnim(target, property, value) {
  new TWEEN.Tween(target)
  .to({property: value}, 1000)
  .yoyo(false)
  .repeat(0)
  .easing(TWEEN.Easing.Sinusoidal.Out)
  .start();
}

scene.add(allGroupp);