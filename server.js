var express = require ('express');
const { writeFile, readFile } = require("fs");
const fs = require('fs');
const path = require('path');
var ip = require('ip');
var socket = require('socket.io');
const open = require('open');

var app = express();
var server = app.listen(4000);
app.use(express.static("public"));

var io = socket(server, { cors: { origin: "http://192.168.1.164:4000", methods: ["GET", "POST"], transports: ['websocket', 'polling'], credentials: true }, allowEIO3: true });
io.of("/ip").on("connection", createJsonDocument);
io.of("/ip").on("connection", sendArrayFiles);
io.of("/ip").on("connection", sendElementData);
io.of("/ip").on("connection", sendChangedPropertys);
io.of("/ip").on("connection", removeElementt);

open( 'http://' + ip.address() + ":4000", function (err) {
  if ( err ) throw err;    
});
console.log(ip.address() + ":4000");

// CREATE NEW JSON FILE -------------------------------------------------------------------------------------------
function createJsonDocument (socket) {
    socket.on('createJsonDocument', function (dataJsonDocument) {
        writeFile('public/data/' + dataJsonDocument + '.json',
`{
    "basic": [],
    "model": []
}`,
        function (err) {
            if (err) throw err;
        });
    });
}



// GET LIST OF ALL JSON FILES -------------------------------------------------------------------------------------------
function sendArrayFiles(socket) {
    let arrayFiles = [];
    const directoryPath = path.join('public/data/');
    fs.readdir(directoryPath, function (err, files) {
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        } 
        files.forEach(function (file) {
            if (file !== "map.json") {
                arrayFiles.push('../data/' + file);
            }
        });
        socket.emit('sendArrayFiles', arrayFiles);
    });
}



// CREATE NEW ELEMENT -------------------------------------------------------------------------------------------
function sendElementData(socket) {
    socket.on('sendElementData', sendElementData);

    function sendElementData(data){
        socket.broadcast.emit('addElement', data);
        const path = 'public/data/' + data.user_identifier + '.json'; 
        readFile(path, (error, dataJson) => {
            if (error) {
                console.log(error);
                return;
            }

            const parsedData = JSON.parse(dataJson);
            parsedData[data.basicOrModel].push(data);
            
            writeFile(path, JSON.stringify(parsedData, null, 2), (err) => {
                if (err) {
                    console.log("Failed to write updated data to file");
                    return;
                }
            });
        });
    }
};



// CHANGE ELEMENT PROPERTY -------------------------------------------------------------------------------------------
function sendChangedPropertys (socket) {
    socket.on('sendChangedPropertys', function (dataProperty) {
        socket.broadcast.emit('changePosition', dataProperty);
        const path = 'public/data/' + dataProperty.user_identifier + '.json'; 
        readFile(path, (error, data) => {
            if (error) {
                console.log(error);
                return;
            }
            
            const parsedData = JSON.parse(data);
            for (let w = 0; w < parsedData[dataProperty.basic_or_model].length; w++) {
                if (parsedData[dataProperty.basic_or_model][w].identifier === dataProperty.identifier) {
                    parsedData[dataProperty.basic_or_model][w][dataProperty.property] = dataProperty.property1;
                }
            }
            
            writeFile(path, JSON.stringify(parsedData, null, 2), (err) => {
                if (err) {
                    console.log("Failed to write updated data to file");
                    return;
                }
            });
        });
    });
}



// REMOVE ELEMENT -------------------------------------------------------------------------------------------
function removeElementt (socket) {
    socket.on('removeElement', function (dataProperty) {
        socket.broadcast.emit('removeElementEverywhere', dataProperty.element_name);
        const path = 'public/data/' + dataProperty.user_identifier + '.json'; 
        readFile(path, (error, data) => {
            if (error) {
                console.log(error);
                return;
            }
            
            const parsedData = JSON.parse(data);
            for (let w = 0; w < parsedData[dataProperty.basic_or_model].length; w++) {
                if (parsedData[dataProperty.basic_or_model][w].identifier === dataProperty.element_name) {
                    parsedData[dataProperty.basic_or_model].splice(w, 1);
                }
            }
            
            writeFile(path, JSON.stringify(parsedData, null, 2), (err) => {
                if (err) {
                    console.log("Failed to write updated data to file");
                    return;
                }
            });
        });
    });
}

// 25 / Octo
// 4 / Nov
// Vendredi 29 14h prépa atelier

// Modif param basic form et modif couleur elements.